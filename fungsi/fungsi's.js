function GetArray2D(row = 1, col = 1) {
    var result = [];
    for (let r = 0; r < row; r++) {
        var resRow = [];
        for (let c = 0; c < col; c++) {
            resRow.push('');
        }
        result.push(resRow);
    }
    return result;
}

function Print(arr) {
    var cnt = '<table class="result">';
    for (let row = 0; row < arr.length; row++) {
        cnt += '<tr>';
        var cols = arr[row];
        for (let col = 0; col < cols.length; col++) {
            cnt += `<td> ${arr[row][col]}</td>`;

        }
        cnt += '</tr>';
    }
    cnt += '</table>';
    return cnt;
}