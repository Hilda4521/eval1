function exam10(n=1) {
    var arr2d = GetArray2D(n, n);
    for (let row = 0; row < n; row++) {
        for (let col = 0; col < n; col++) {
            if ((row == 0 || row == n - 1) && (col == 0 || col == n - 1)) {
                arr2d[row][col] = " ";
            } else {
                arr2d[row][col] = "*";
            }
        }
        
    }
    return Print (arr2d);
}