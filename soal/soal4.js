function exam04(kata = "aku") {
    var berubah = kata.split(' ');
    var arrkata = [];
    for (let i = 0; i < berubah.length; i++) {
        var newkata = '';
        var s = berubah[i];
        for (let j = 0; j < s.length; j++) {
            //untuk mencari kata tengah
            if (j > 0 && j < s.length - 1) {
                if (j > 0 && j <= 1) {
                    newkata += "***";
                }
            } else {
                newkata += s[j];
            }
        }
        arrkata[i] = newkata;
    }

    return arrkata.join(" ");
}